<?php
 /**
 * Template Name: Contact Page
 *
 * @package Dahlia_Dowler
 */

get_header();
?>

	<div id="primary" class="content-area contact">

		<main id="main" class="site-main">

		<?php if ( get_field( "page_title" ) ) : ?>

			<div class="title curveb flex-column flex-end-center z8">

				<div class="text-<?php if ( get_field( "page_title_size" ) ) the_field( "page_title_size" ); ?> full text-center">

					<h1><?php the_title(); ?></h1>

				</div>

			</div>

		<?php endif; ?>

		<div class="contact-methods full flex-row flex-space-between-center">

			<?php	

				if ( have_rows( "contact_methods" ) ) : 
				while ( have_rows( "contact_methods" ) ) : the_row();

			?>

			<article class="contact-method card box-shadow padding flex-row flex-wrap flex-space-between-space-between">

				<h2><?php the_sub_field( "contact_method_name" ); ?></h2>

				<span class="book-a-session-icon book-a-session-icon-phone"></span>

				<p><?php  the_sub_field( "contact_method_content" ); ?></p>

				<?php if ( get_sub_field( "contact_method_button" ) ) : ?>

					<a class="animated background-gradient button" href="<?php 
					
					if ( get_sub_field( "contact_method_type" ) === "phone" && get_sub_field( "contact_method_phone" ) ) :

						$phone = get_sub_field( "contact_method_phone" );

						echo "tel:$phone";

					elseif ( get_sub_field( "contact_method_type" ) === "email" && get_sub_field( "contact_method_email" ) ) :
						
						$email = get_sub_field( "contact_method_email" );

						echo "mailto:$email";
					
					elseif ( get_sub_field( "contact_method_page_link" ) ) : the_sub_field( "contact_method_page_link" );

					elseif ( get_sub_field ( "contact_method_other_link" ) ) : the_sub_field( "contact_method_other_link" );

					endif;

					?>" target="_blank"><?php the_sub_field( "contact_method_button_text" ); ?></a>

				<?php endif; ?>

			</article>

			<?php endwhile; endif; ?>

		</div>

		<?php if ( ! empty ( get_the_content() && ! is_wp_error( get_the_content() ) ) ) : ?> <div class="contact-form-container full card box-shadow padding flex-row flex-wrap flex-space-between-space-between"><h2>Send me a message</h2><span class="book-a-session-icon book-a-session-icon-mail_outline"></span><?php the_content(); ?></div><?php endif; ?>

		<div class="bar bar-large background-gradient box-shadow newsletter-signup"><div class="full flex-row flex-wrap padding flex-space-between-space-between"><?php echo "<h2>Signup to the newsletter</h2>" . do_shortcode( '[contact-form-7 id="606" title="Newsletter signup"]' ); ?></div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();

