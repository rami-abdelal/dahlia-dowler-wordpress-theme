<?php
 /**
 * Template Name: FAQ Page
 *
 * @package Dahlia_Dowler
 */

get_header();
?>

	<div id="primary" class="content-area faq">

		<main id="main" class="site-main">

		<?php if ( get_field( "page_title" ) ) : ?>

			<div class="title curveb flex-column flex-end-center z8">

				<div class="text-<?php if ( get_field( "page_title_size" ) ) the_field( "page_title_size" ); ?> full text-center">

					<h1><?php the_title(); ?></h1>

				</div>

			</div>

		<?php endif; ?>

		<div class="faq-items">

			<?php	

				if ( have_rows( "faq_items" ) ) : 
				while ( have_rows( "faq_items" ) ) : the_row();

			?>

			<article class="faq-item card box-shadow full text-left">

				<header class="padding flex-row flex-center-center"><h2 class="text-gradient"><?php the_sub_field( "question" ); ?></h2></header>

				<p class="padding text-center"><?php  the_sub_field( "answer" ); ?></p>

			</article>

			<?php endwhile; endif; ?>

		</div>

		<div class="bar background-gradient box-shadow"><div class="full flex-row padding flex-space-between-center"><p>Book a session now</p><a href="book-a-session" class="animated button light-button">Book A Session</a></div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();

