/**
 * File script.js.
 *
 * Handles toggling the navigation menu for small screens and other stuff later
 */
jQuery(document).ready(function( $ ) {

	$('.hamburger').on("click", function(e){

		if ( $(this).hasClass('is-active') ) {

			// Close menu

			$(this).removeClass('is-active');

			$('#mobile-menu-container').removeClass('open');

		} else {

			// Open menu

			$(this).addClass('is-active');

			$('#mobile-menu-container').addClass('open');

		}

	});


});