<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dahlia_Dowler
 */

get_header();
?>

	<div id="primary" class="content-area">

		<main id="main" class="site-main">



		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
				the_archive_title( '<div class="title curveb flex-column flex-end-center z8"><div class="full text-center"><h1>', '</h1></div></div>' );
				the_archive_description( '<div class="curveb flex-column flex-end-center z7 archive-description-container"><div class="slim text-center archive-description">', '</div></div>' );
				?>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
