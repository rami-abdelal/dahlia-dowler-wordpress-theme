<?php
 /**
 * Template Name: About Page
 *
 * @package Dahlia_Dowler
 */

get_header();
?>

	<div id="primary" class="content-area about">

		<main id="main" class="site-main">

		<?php if ( get_field( "page_title" ) ) : ?>

			<div class="title curveb flex-column flex-end-center z8">

				<div class="text-<?php if ( get_field( "page_title_size" ) ) the_field( "page_title_size" ); ?> full text-center">

					<h1><?php the_title(); ?></h1>

				</div>

			</div>

		<?php endif; ?>

		<div class="about-sections">

			<?php	

				if ( have_rows( "about_section" ) ) : 
					$count = 8;
				while ( have_rows( "about_section" ) ) : the_row();
					$count--;

			?>

			<article class="about-section curveb text-left z<?php echo $count; ?>" id="<?php the_sub_field( "section_id" ) ?>" style="background-image:url(<?php the_sub_field( "section_background_image" ); ?>);">

				<?php if ( get_sub_field( "section_pre-title_subtitle" ) ) : ?>
				
					<h3 class="subtitle slim padding text-left"><?php the_sub_field( "section_pre-title_subtitle" ); ?></h3>

				<?php endif; ?>

				<h2 class="slim padding text-left"><?php the_sub_field( "section_title" ); ?></h2>

				<?php if ( get_sub_field( "section_post-title_subtitle" ) ) : ?>
				
					<h3 class="subtitle slim padding text-left"><?php the_sub_field( "section_post-title_subtitle" ); ?></h3>

				<?php endif; ?>

				<?php	

				if ( have_rows( "section_text_content" ) ) : 
				while ( have_rows( "section_text_content" ) ) : the_row();

				?>

					<p class="padding slim text-left"><?php  the_sub_field( "section_paragraph" ); ?></p>


				<?php endwhile; endif; ?>


			</article>

			<?php endwhile; endif; ?>

		</div>

		<div class="bar background-gradient"><div class="full flex-row padding flex-space-between-center"><p>Book a session now</p><a href="book-a-session" class="animated button light-button">Book A Session</a></div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();

