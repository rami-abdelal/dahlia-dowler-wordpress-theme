<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dahlia_Dowler
 */

?>
<?php if ( get_field( "page_title" ) ) : ?>

	<div class="title curveb flex-column flex-end-center z8">

		<div class="text-<?php if ( get_field( "page_title_size" ) ) the_field( "page_title_size" ); ?> full text-center">

			<h1><?php the_title(); ?></h1>

		</div>

	</div>

<?php endif; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	</header><!-- .entry-header -->

	<?php dahlia_dowler_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'dahlia-dowler' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>

		<footer class="bar entry-footer background-gradient">
			<div class="full flex-row flex-start-center padding">
				<?php
				edit_post_link(
					sprintf(
						wp_kses(
							/* translators: %s: Name of current post. Only visible to screen readers */
							__( 'Edit <span class="screen-reader-text">%s</span>', 'dahlia-dowler' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					),
					'<span class="edit-link">',
					'</span>'
				);
				?>
			</div>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
