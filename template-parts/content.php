<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dahlia_Dowler
 */

?>

<?php 

$post_class = is_singular() ? array( "post-single" ) : array( "post-multiple", "card", "box-shadow", "full" );
$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "full" )[0];

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $post_class ); ?>>
	<header class="entry-header<?php if ( 'post' === get_post_type() ) echo " has-meta"; if ( ! is_singular() ) echo " post-image"; ?>"<?php if ( ! is_singular() && $image ) echo " style='background-image:url($image)'"; ?>>
		<?php
		if ( is_singular() ) :
			the_title( '<div class="curve flex-column flex-end-center z8" style="background-image:url(' . $image . ');"><div class="scrim"><div class="inner-curve"><h1 class="full text-center">', '</h1></div></div></div>' );
		else :
			echo '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark" class="scrim padding flex-colum flex-start-end">';
			the_title( '<h2 class="entry-title">', '</h2></a>' );
		endif;

		if ( 'post' === get_post_type() && is_singular() ) :
			?>
			<div class="entry-meta curveb z7">
				<div class="full text-center">
					<?php
					dahlia_dowler_posted_on();
					dahlia_dowler_posted_by();
					?>
				</div>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->
	
	<div class="entry-content<?php echo is_singular() ? "-single curvea z6" : "-multiple flex-row flex-height-stretch"; ?>">
		<?php echo is_singular() ? '<div class="slim inner-curve">' : "<div class='post-preview padding'>" ?>
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'dahlia-dowler' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'dahlia-dowler' ),
			'after'  => '</div>',
		) );
		
		?></div><?php

		if ( ! is_singular() ) : ?>

			<div class='entry-meta flex-row flex-wrap flex-space-between-end padding'>
				<a href="<?php the_permalink(); ?>" class="animated button cicle-button flex-column flex-center-center background-gradient">
					<span class="dahlia-dowler-icon dahlia-dowler-icon-read"></span>
					<span class="read">Read</span>
				</a>
				<?php dahlia_dowler_entry_categories(); dahlia_dowler_posted_on(); dahlia_dowler_posted_by(); ?>
			</div>

		<?php endif; ?>


	</div><!-- .entry-content -->
	<?php if ( is_singular() ) : ?>
	<footer class="entry-footer z4 curveb">
		<div class="full padding flex-column flex-space-between-center">
			<?php dahlia_dowler_entry_footer(); ?>
		</div>
	</footer><!-- .entry-footer -->
		<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
