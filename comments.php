<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dahlia_Dowler
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area full padding">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
		<h2 class="comments-title text-center full padding">
			<?php
			$dahlia_dowler_comment_count = get_comments_number();
			if ( '1' === $dahlia_dowler_comment_count ) {
				printf(
					/* translators: 1: title. */
					esc_html__( 'One comment on &ldquo;%1$s&rdquo;', 'dahlia-dowler' ),
					'<span>' . get_the_title() . '</span>'
				);
			} else {
				printf( // WPCS: XSS OK.
					/* translators: 1: comment count number, 2: title. */
					esc_html( _nx( '%1$s comment on &ldquo;%2$s&rdquo;', '%1$s comments on &ldquo;%2$s&rdquo;', $dahlia_dowler_comment_count, 'comments title', 'dahlia-dowler' ) ),
					number_format_i18n( $dahlia_dowler_comment_count ),
					'<span>' . get_the_title() . '</span>'
				);
			}
			?>
		</h2><!-- .comments-title -->

		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php
			wp_list_comments( array(
				'style'      => 'ol',
				'short_ping' => true,
				'avatar_size' => 128
			) );
			?>
		</ol><!-- .comment-list -->

		<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'dahlia-dowler' ); ?></p>
			<?php
		endif;

	endif; // Check for have_comments().

	comment_form( 

		array(

			"logged_in_as" => '<div class="logged-in-comment-message">What does <span class="commenter-display-name">' . get_userdata( get_current_user_id() )->display_name . '</span> have to say?</div><a class="animated button light-button low-button not-you-logout">Not you? Logout</a>',
			"comment_field" => '<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" required="required" placeholder="Start typing here..."></textarea></p>',
			"title_reply"	=> "",
		)

	);
	?>

</div><!-- #comments -->
