<?php
 /**
 * Template Name: Home Page
 *
 * @package Dahlia_Dowler
 */

get_header( "transparent" );
?>

	<div id="primary" class="content-area home">

		<main id="main" class="site-main">

		<div class="home-hero curveb flex-column flex-end-center z8" style="background-image:url(<?php  the_field( "home_hero_image" ); ?>);">

			<div class="full text-center">

				<h1><?php the_field( "home_hero_title" ); ?></h1>

				<?php if ( get_field( "home_hero_subtitle" ) ) : ?>
					<h3 class="subtitle"> <?php the_field( "home_hero_subtitle" ) ?></h3>
				<?php endif; ?>

				<a href="<?php the_field( "home_hero_button_link" ) ?>" class="animated background-gradient button">
				<?php the_field( "home_hero_button_text" ) ?>
				</a>

			</div>

		</div>

		<div class="home-intro curvea curveb-box-shadow flex-column flex-end-center z7">

			<div class="narrow text-center">

				<h2 class="text-gradient"><?php the_field( "home_introduction_title" ); ?></h2>

				<h4 class="subtitle"><?php the_field( "home_introduction_subtitle" ); ?></h4>

				<p><?php  the_field( "home_introduction_paragraph_text" ); ?></p>

				<a href="<?php the_field( "home_introduction_button_link" ); ?>" class="animated background-gradient button"><?php the_field( "home_introduction_button_text" ); ?></a>

			</div>

		</div>

		<?php echo dahlia_dowler_get_posts( false, 3 ); ?>
		<div class="bar background-gradient box-shadow"><div class="full flex-row padding flex-space-between-center"><p>Check out our latest posts</p><a href="blog" class="animated button light-button">Blog</a></div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();

