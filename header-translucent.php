<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Dahlia_Dowler
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'dahlia-dowler' ); ?></a>

	<header id="masthead" class="site-header">

		<nav id="site-navigation" class="main-navigation wrap flex-row flex-center-center translucent curveb z9">

			<!-- Mobile menu -->

			<div id="mobile-menu-container">
				
				<a href="<?php echo get_home_url(); ?>" class="custom-logo-link">
					<img src="<?= get_template_directory_uri() . '/img/Horizontal-Slim.png' ?>" alt="<?php echo get_bloginfo( "name" ) . " Logo"; ?>">
				</a>

				<?php wp_nav_menu( array( 'theme_location' => 'mobile_menu' ) ); ?>		

				<button class="hamburger hamburger--spring" type="button">
					<span class="hamburger-box"><span class="hamburger-inner"></span></span>
				</button>

			</div>

			<div class="logo flex-row flex-center-center">
			<?php

			wp_nav_menu( array(
				'theme_location' => 'primary_left_menu',
			) );
			?>

			<a href="<?= get_home_url(); ?>" class="custom-logo-link">
				<img src="<?= get_template_directory_uri() . '/img/Vertical.png' ?>" alt="<?= get_bloginfo( "name" ) ?> Logo">
			</a>
			
			<?php
			wp_nav_menu( array(
				'theme_location' => 'primary_right_menu',
			) );
			?>
			</div>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
