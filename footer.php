<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Dahlia_Dowler
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="curveb z1">
			<div class="full flex-row flex-center-center">

			<a href="<?= home_url(); ?>">
				<img src="<?= get_template_directory_uri() . '/img/Vertical-white.png' ?>" alt="<?= get_bloginfo( 'name' ) ?> Logo">
			</a>
			</div>
		</div>
		<div class="flex-row flex-wrap full padding">
		<?php

		wp_nav_menu( array(
			'theme_location' => 'footer_left_menu',
		) );
		wp_nav_menu( array(
			'theme_location' => 'footer_centre_menu',
		) );
		wp_nav_menu( array(
			'theme_location' => 'footer_right_menu',
		) );

		?>
		</div><!-- inner -->
		<div class="site-info padding">
			<div class="full text-center">
				<?php echo get_bloginfo( "name" ); ?> &copy; <?php echo date( "Y" ); ?>. All Rights Reserved.
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
